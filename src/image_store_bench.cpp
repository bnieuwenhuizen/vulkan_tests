/*
 * Copyright © 2020 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <stdio.h>
#include <time.h>

double gettime() {
   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC, &ts);
   return ts.tv_sec + ts.tv_nsec/1e9;
}

int main() {
   Device dev;
   VkResult result;
   const VkDescriptorSetLayoutBinding bindings[] = {
      {
         .binding = 0,
         .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
         .descriptorCount = 1,
         .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      },
      {
         .binding = 1,
         .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
         .descriptorCount = 1,
         .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      }
   };
   const VkDescriptorSetLayoutCreateInfo ds_layout_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = 2,
      .pBindings = bindings
   };
   VkDescriptorSetLayout ds_layout;
   result = vkCreateDescriptorSetLayout(dev.dev(),& ds_layout_info, NULL, &ds_layout);
   if (result != VK_SUCCESS)
      throw -1;

   VkPushConstantRange pc_range = {
      .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
      .offset = 0,
      .size = 12
   };
   VkPipelineLayout pl;
   const VkPipelineLayoutCreateInfo pl_create_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 1,
      .pSetLayouts = &ds_layout,
      .pushConstantRangeCount = 1,
      .pPushConstantRanges = &pc_range,
   };
   result = vkCreatePipelineLayout(dev.dev(), &pl_create_info, nullptr, &pl);
   if (result != VK_SUCCESS)
      throw -1;

   const std::string common_source = R"(
      #version 450
      layout (set = 0, binding = 0, rgba8) coherent uniform image2D out_img;
      layout (set = 0, binding = 1) uniform sampler2D in_sampler;

      layout(push_constant) uniform PushConsts {
         vec2 coord_div;
         uint flags;
      } pushConsts;

      void process(ivec2 coord) {
         vec4 color = vec4(0.0, 1.0, 0.0, 1.0);
         if ((pushConsts.flags & 1) != 0) {
            color += texelFetch(in_sampler, coord, 0);
         } else if ((pushConsts.flags & 2) != 0) {
            color += texelFetch(in_sampler, coord, 0) + texelFetch(in_sampler, coord - ivec2(3, 0), 0) + texelFetch(in_sampler, coord - ivec2(0, 3), 0);
         } else if ((pushConsts.flags & 4) != 0) {
            color += texelFetch(in_sampler, coord, 0) + texelFetch(in_sampler, coord - ivec2(3, 0), 0) + texelFetch(in_sampler, coord - ivec2(0, 3), 0) + texelFetch(in_sampler, coord + ivec2(3, 0), 0) + texelFetch(in_sampler, coord + ivec2(0, 3), 0);
         }
         imageStore(out_img, coord, color);
      }
   )";
   auto pipeline_8x8 = create_compute_pipeline(dev, pl, common_source + R"(
      layout (local_size_x = 8, local_size_y = 8) in;
      void main() {
         uvec2 base = gl_WorkGroupID.xy * uvec2(32,16);
         process(ivec2(base + uvec2(0, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(8, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(8, 8) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(0, 8) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(24, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(24, 8) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 8) + gl_LocalInvocationID.xy));
      }
   )");
   auto pipeline_16x4 = create_compute_pipeline(dev, pl, common_source + R"(
      layout (local_size_x = 16, local_size_y = 4) in;
      void main() {
         uvec2 base = gl_WorkGroupID.xy * uvec2(32,16);
         process(ivec2(base + uvec2(0, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(0, 4) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(0, 8) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(0, 12) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 4) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 8) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 12) + gl_LocalInvocationID.xy));
      }
   )");
   auto pipeline_4x16 = create_compute_pipeline(dev, pl, common_source + R"(
      layout (local_size_x = 4, local_size_y = 16) in;
      void main() {
         uvec2 base = gl_WorkGroupID.xy * uvec2(32,16);
         process(ivec2(base + uvec2(0, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(4, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(8, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(12, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(16, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(20, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(24, 0) + gl_LocalInvocationID.xy));
         process(ivec2(base + uvec2(28, 0) + gl_LocalInvocationID.xy));
      }
   )");
   VkDescriptorPoolSize ds_pool_sizes[] = {
      {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 128},
      {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 128},
      {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 128},
      {VK_DESCRIPTOR_TYPE_SAMPLER, 128},
      {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 128},
      {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 128},
   };
   VkDescriptorPoolCreateInfo ds_pool_create_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
      .maxSets = 128,
      .poolSizeCount = 6,
      .pPoolSizes = ds_pool_sizes
   };

   VkDescriptorPool ds_pool;
   result = vkCreateDescriptorPool(dev.dev(), &ds_pool_create_info, NULL, &ds_pool);
   if (result != VK_SUCCESS)
      throw -1;

   VkSamplerCreateInfo sampler_create_info = {
      .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
      .magFilter = VK_FILTER_LINEAR,
      .minFilter = VK_FILTER_LINEAR,
      .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
      .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
      .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
   };
   VkSampler sampler;
   result = vkCreateSampler(dev.dev(), &sampler_create_info, NULL, &sampler);
   if (result != VK_SUCCESS)
      throw -1;

   struct Test {
      unsigned width;
      unsigned height;
      VkPipeline pipeline;
      const char *name;
      unsigned perf_div;
   } tests[] = {
      {4096, 4096, pipeline_8x8, "VRAM (8x8)", 4},
      {4096, 4096, pipeline_16x4, "VRAM (16x4)", 4},
      {4096, 4096, pipeline_4x16, "VRAM (4x16)", 4},
      {512, 256, pipeline_8x8, "L2 (8x8)", 1},
      {512, 256, pipeline_16x4, "L2 (16x4)", 1},
      {512, 256, pipeline_4x16, "L2 (4x16)", 1},
   };
   for (const auto& test : tests) {
      const unsigned wg_width = 32;
      const unsigned wg_height = 16;
      const unsigned d = 320 * (4096 / test.width) * (4096 / test.height) / test.perf_div;

   const VkImageCreateInfo src_img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_R8G8B8A8_UNORM,
         .extent = {test.width, test.height, 1},
         .mipLevels = 1,
         .arrayLayers = 1,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_OPTIMAL,
         .usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      };

      Image src_image(dev, src_img_create_info, Access::gpu);
      const VkImageCreateInfo img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_R8G8B8A8_UNORM,
         .extent = {test.width, test.height, 1},
         .mipLevels = 1,
         .arrayLayers = 1,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_OPTIMAL,
         .usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      };

      Image image(dev, img_create_info, Access::gpu);

      VkDescriptorSetAllocateInfo ds_info = {
         .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
         .descriptorPool = ds_pool,
         .descriptorSetCount = 1,
         .pSetLayouts = &ds_layout,
      };
      VkDescriptorSet ds;
      result = vkAllocateDescriptorSets(dev.dev(), &ds_info, &ds);
      if (result != VK_SUCCESS)
         throw -1;

      const VkDescriptorImageInfo ds_image_info = {
         .imageView = image.view(),
         .imageLayout = VK_IMAGE_LAYOUT_GENERAL
      };
      const VkDescriptorImageInfo ds_src_image_info = {
         .sampler = sampler,
         .imageView = src_image.view(),
         .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      };
      VkWriteDescriptorSet ds_writes[] = {
         {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = ds,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
            .pImageInfo = &ds_image_info
         },
         {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = ds,
            .dstBinding = 1,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = &ds_src_image_info
         }
      };

      vkUpdateDescriptorSets(dev.dev(), 2, ds_writes, 0, NULL);

      struct {
         const char *name;
         unsigned flags;
      } fetch_infos[] = {
         {"0 fetches", 0},
         {"1 fetch  ", 1},
         {"3 fetches", 2},
         {"5 fetches", 4},
      };
      for (const auto& fetch_info : fetch_infos) {
         std::vector<VkCommandBuffer> cmd_buffers(4);
         for (auto& cmd_buf : cmd_buffers) {
            cmd_buf = create_cmdbuf(dev, dev.gfx_pool());
            
            const VkCommandBufferBeginInfo begin_info = {
               .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
            };
            vkBeginCommandBuffer(cmd_buf, &begin_info);

            VkImageMemoryBarrier image_barriers[] = {
               {
                  .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                  .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
                  .dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT | VK_ACCESS_SHADER_READ_BIT,
                  .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                  .newLayout = VK_IMAGE_LAYOUT_GENERAL,
                  .srcQueueFamilyIndex = 0,
                  .dstQueueFamilyIndex = 0,
                  .image = image.image(),
                  .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
               },
               {
                  .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                  .srcAccessMask = 0,
                  .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                  .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                  .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                  .srcQueueFamilyIndex = 0,
                  .dstQueueFamilyIndex = 0,
                  .image = image.image(),
                  .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
               }
            };
            if (cmd_buf == cmd_buffers[0]) {
               vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, NULL, 0, NULL, 2, image_barriers);
               VkClearColorValue color = {
                  .float32 = {0.0, 0.0, 0.0, 0.0}
               };
               VkImageSubresourceRange range = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
               vkCmdClearColorImage(cmd_buf, src_image.image(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &color, 1, &range);

               image_barriers[1].srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
               image_barriers[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
               image_barriers[1].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
               image_barriers[1].newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
               vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, NULL, 0, NULL, 1, image_barriers + 1);
            }
            vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, test.pipeline);
            vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pl, 0, 1, &ds, 0, NULL);

            struct {
               float w_div;
               float h_div;
               uint32_t flags;
            } pc = {
               1.0f/test.width,
               1.0f/test.height,
               fetch_info.flags
            };
            vkCmdPushConstants(cmd_buf, pl, VK_SHADER_STAGE_COMPUTE_BIT, 0, 12, &pc);

            unsigned w = (test.width + wg_width - 1) / wg_width;
            unsigned h = (test.height + wg_height - 1) / wg_height;
            vkCmdDispatch(cmd_buf, w, h, d);
            if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
               throw -1;
         }


         std::vector<VkFence> fences(cmd_buffers.size());
         for (auto& f : fences) {
            const VkFenceCreateInfo fence_info = {
               .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
               .flags = VK_FENCE_CREATE_SIGNALED_BIT,
            };
            VkResult result = vkCreateFence(dev.dev(), &fence_info, nullptr, &f);
            if (result != VK_SUCCESS)
               throw -1;
         }

         unsigned idx = 0;
         std::vector<double> times(cmd_buffers.size());
         double total_rate = 0.0, rate_count = 0.0;
         double start_time = gettime();
         for (unsigned i = 0; i < 1000; ++i) {

            VkResult result = vkWaitForFences(dev.dev(), 1, &fences[idx], true, 1000000000);
            if (result != VK_SUCCESS)
               throw -1;

            double t = gettime();
            if (times[idx]  > 0.0) {
               double diff = t - times[idx];
               double rate = (double)test.width * test.height * d * cmd_buffers.size() / diff;
               total_rate += rate;
               rate_count += 1.0;
            }
            times[idx] = t;
            if (t - start_time > 3.0)
               break;
            vkResetFences(dev.dev(), 1, &fences[idx]);

            const VkSubmitInfo submit_info = {
               .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
               .commandBufferCount = 1,
               .pCommandBuffers = &cmd_buffers[idx]
            };
            vkQueueSubmit(dev.gfx_queue(), 1, &submit_info, fences[idx]);
            idx = (idx + 1) % cmd_buffers.size();
         }
         double avg_rate = total_rate / rate_count;
         fprintf(stdout, "%s (%s): rate: %f Gpixel/s (uncompressed bandwidth: %f GiB/s, footprint: %f MiB)\n", test.name, fetch_info.name, avg_rate / 1e9, avg_rate*4/(1u << 30), (double)image.mem_size()/ (1u << 20));
      }
   }
   return 0;
}
