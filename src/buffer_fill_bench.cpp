/*
 * Copyright © 2020 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <stdio.h>
#include <time.h>

double gettime() {
   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC, &ts);
   return ts.tv_sec + ts.tv_nsec/1e9;
}

int main() {
   Device dev;

   std::size_t size = 256 * 1024 * 1024;
   std::size_t fill_count = 100;
   Buffer src_buf(dev, size, Access::gpu);
   Buffer dst_buf(dev, size, Access::gpu);

   std::vector<VkCommandBuffer> cmd_buffers(16);
   for (auto& cmd_buf : cmd_buffers) {
      cmd_buf = create_cmdbuf(dev, dev.gfx_pool());

      const VkCommandBufferBeginInfo begin_info = {
         .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
      };
      vkBeginCommandBuffer(cmd_buf, &begin_info);

      for (uint32_t n = 0; n < fill_count; n++)
         vkCmdFillBuffer(cmd_buf, dst_buf.buffer(), 0, size, 0);

      if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
         throw -1;
   }


   std::vector<VkFence> fences(cmd_buffers.size());
   for (auto& f : fences) {
      const VkFenceCreateInfo fence_info = {
         .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
         .flags = VK_FENCE_CREATE_SIGNALED_BIT,
      };
      VkResult result = vkCreateFence(dev.dev(), &fence_info, nullptr, &f);
      if (result != VK_SUCCESS)
         throw -1;
   }

   unsigned idx = 0;
   std::vector<double> times(cmd_buffers.size());
   for (unsigned i = 0; i < 1000; ++i) {

      VkResult result = vkWaitForFences(dev.dev(), 1, &fences[idx], true, 1000000000);
      if (result != VK_SUCCESS)
         throw -1;

      double t = gettime();
      if (times[idx]  > 0.0) {
         double diff = t - times[idx];
         double bytes = (double)size * fill_count * cmd_buffers.size();
         fprintf(stderr, "time for full iter: %f s perf: %f GiB/s (%f GB/s)\n", diff, bytes/diff/(1024*1024*1024), bytes/diff/1e9);
      }
      times[idx] = t;
      vkResetFences(dev.dev(), 1, &fences[idx]);

      const VkSubmitInfo submit_info = {
         .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
         .commandBufferCount = 1,
         .pCommandBuffers = &cmd_buffers[idx]
      };
      vkQueueSubmit(dev.gfx_queue(), 1, &submit_info, fences[idx]);
      idx = (idx + 1) % cmd_buffers.size();
   }

   printf("success!\n");
   return 0;
}
