/*
 * Copyright © 2020 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <stdio.h>
#include <time.h>

double gettime() {
   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC, &ts);
   return ts.tv_sec + ts.tv_nsec/1e9;
}

int main() {
   Device dev;

   std::size_t size = 128 * 1024 * 1024;
   Buffer src_buf(dev, size, Access::gpu);

      const VkImageCreateInfo img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_BC4_UNORM_BLOCK,
         .extent = {32,32, 1},
         .mipLevels = 6,
         .arrayLayers = 6,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_OPTIMAL,
         .usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      };
   Image img(dev, img_create_info, Access::gpu);
   fprintf(stderr, "mem_size: %lu\n", (uint64_t)img.mem_size());
   Buffer dst_buf(dev, size, Access::gpu);

   std::vector<VkCommandBuffer> cmd_buffers(16);
   for (auto& cmd_buf : cmd_buffers) {
      cmd_buf = create_cmdbuf(dev, dev.gfx_pool());

      const VkCommandBufferBeginInfo begin_info = {
         .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
      };
      vkBeginCommandBuffer(cmd_buf, &begin_info);


#define MAX2(a,b) (((a) < (b)) ? (b) : (a))
      for (unsigned l = 0; l < 6; ++l) {
         for (unsigned m = 0; m < 6; ++m) {
            VkBufferImageCopy region = {
             .imageSubresource = {.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .mipLevel = m, .baseArrayLayer = l, .layerCount = 1},
             .imageExtent = {MAX2(32u >> m, 1u), MAX2(32u >> m, 1u), 1}
            };
            vkCmdCopyBufferToImage(cmd_buf, src_buf.buffer(), img.image(), VK_IMAGE_LAYOUT_GENERAL, 1, &region);
         }
      }
      if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
         throw -1;
   }


   std::vector<VkFence> fences(cmd_buffers.size());
   for (auto& f : fences) {
      const VkFenceCreateInfo fence_info = {
         .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
         .flags = VK_FENCE_CREATE_SIGNALED_BIT,
      };
      VkResult result = vkCreateFence(dev.dev(), &fence_info, nullptr, &f);
      if (result != VK_SUCCESS)
         throw -1;
   }

   unsigned idx = 0;
   std::vector<double> times(cmd_buffers.size());
   for (unsigned i = 0; i < 1000; ++i) {

      VkResult result = vkWaitForFences(dev.dev(), 1, &fences[idx], true, 1000000000);
      if (result != VK_SUCCESS)
         throw -1;


      vkResetFences(dev.dev(), 1, &fences[idx]);

      const VkSubmitInfo submit_info = {
         .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
         .commandBufferCount = 1,
         .pCommandBuffers = &cmd_buffers[idx]
      };
      vkQueueSubmit(dev.gfx_queue(), 1, &submit_info, fences[idx]);
      idx = (idx + 1) % cmd_buffers.size();
   }

   printf("success!\n");
   return 0;
}
