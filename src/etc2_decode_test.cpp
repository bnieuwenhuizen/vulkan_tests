/*
 * Copyright © 2021 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <time.h>

double gettime() {
   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC, &ts);
   return ts.tv_sec + ts.tv_nsec/1e9;
}

struct ImageData {
   unsigned width;
   unsigned height;
   std::vector<char> data;
};

typedef char Byte;
typedef uint32_t UInt32;
typedef uint64_t UInt64;

struct ktx_header {
	Byte identifier[12];
	UInt32 endianness;
	UInt32 glType;
	UInt32 glTypeSize;
	UInt32 glFormat;
	UInt32 glInternalFormat;
	UInt32 glBaseInternalFormat;
	UInt32 pixelWidth;
	UInt32 pixelHeight;
	UInt32 pixelDepth;
	UInt32 numberOfArrayElements;
	UInt32 numberOfFaces;
	UInt32 numberOfMipmapLevels;
	UInt32 bytesOfKeyValueData;
};

ImageData read_ktx(std::string filename)
{
	std::ifstream in(filename);
	struct ktx_header header;

	in.read((char*)&header, sizeof(header));
	if (header.numberOfArrayElements > 1) {
		std::cerr << "Unsupported layer count " << header.numberOfArrayElements << "\n";
		std::exit(1);
	}
	if (header.numberOfMipmapLevels > 1) {
		std::cerr << "Unsupported level count " << header.numberOfMipmapLevels << "\n";
		std::exit(1);
	}
	if (header.numberOfFaces > 1) {
		std::cerr << "Unsupported face count " << header.numberOfFaces << "\n";
		std::exit(1);
	}
	if (header.pixelDepth > 1) {
		std::cerr << "Unsupported depth " << header.pixelDepth << "\n";
		std::exit(1);
	}

	std::vector<char> resv(header.bytesOfKeyValueData);
	in.read(resv.data(), resv.size());

	uint32_t imageSize;
	in.read((char*)&imageSize, sizeof(imageSize));
	std::vector<char> data(imageSize);
	in.read(data.data(), data.size());
	std::cout << imageSize << " " << (header.pixelWidth * header.pixelHeight / 16 * 8) << "\n";
	return ImageData {header.pixelWidth, header.pixelHeight, data};
}

int main(int argc, char *argv[]) {
   Device dev;
   if (argc < 2) {
	   std::cerr << "not enough arguments\n";
	   return 1;
   }
   ImageData image_data = read_ktx(argv[1]);

   Buffer src_buf(dev, image_data.data.size(), Access::host);
   memcpy(src_buf.data(), image_data.data.data(), image_data.data.size());

      const VkImageCreateInfo src_img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .flags = 0,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK,
         .extent = {image_data.width,image_data.height, 1},
         .mipLevels = 1,
         .arrayLayers = 1,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_OPTIMAL,
         .usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      };
   Image src_img(dev, src_img_create_info, Access::gpu);

      const VkImageCreateInfo dst_img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .flags = 0,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_R8G8B8A8_UNORM,
         .extent = {image_data.width,image_data.height, 1},
         .mipLevels = 1,
         .arrayLayers = 1,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_OPTIMAL,
         .usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      };
   Image dst_img(dev, dst_img_create_info, Access::gpu);


   const std::size_t dst_size = image_data.width * image_data.height * 4;
   Buffer dst_buf(dev, dst_size, Access::host);

   VkCommandBuffer cmd_buf = create_cmdbuf(dev, dev.gfx_pool());
   const VkCommandBufferBeginInfo begin_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
   };
   vkBeginCommandBuffer(cmd_buf, &begin_info);

   VkImageMemoryBarrier src_init_barriers[2] = {
      {
         .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
         .srcAccessMask = 0,
         .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
         .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
         .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
         .srcQueueFamilyIndex = 0,
         .dstQueueFamilyIndex = 0,
         .image = src_img.image(),
         .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
      }, {
         .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
         .srcAccessMask = 0,
         .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
         .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
         .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
         .srcQueueFamilyIndex = 0,
         .dstQueueFamilyIndex = 0,
         .image = dst_img.image(),
         .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
      }
   };
   
   vkCmdPipelineBarrier(cmd_buf, 0, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 2, src_init_barriers);

   const VkBufferImageCopy src_copy = {
      .imageSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
      .imageExtent = {image_data.width, image_data.height, 1},
   };

   vkCmdCopyBufferToImage(cmd_buf, src_buf.buffer(), src_img.image(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &src_copy);

   VkImageMemoryBarrier transition_barriers[2] = {
      {
         .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
         .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
         .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
         .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
         .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
         .srcQueueFamilyIndex = 0,
         .dstQueueFamilyIndex = 0,
         .image = src_img.image(),
         .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
      },
      {
         .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
         .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
         .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
         .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
         .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
         .srcQueueFamilyIndex = 0,
         .dstQueueFamilyIndex = 0,
         .image = dst_img.image(),
         .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
      }
   };

   vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, transition_barriers);

   const VkImageBlit blit_region = {
      .srcSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
      .srcOffsets = {{0, 0, 0}, {(int32_t)image_data.width, (int32_t)image_data.height, 1}},
      .dstSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
      .dstOffsets = {{0, 0, 0}, {(int32_t)image_data.width, (int32_t)image_data.height, 1}},
   };
   vkCmdBlitImage(cmd_buf, src_img.image(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dst_img.image(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit_region, VK_FILTER_NEAREST);

   vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, transition_barriers + 1);

   const VkBufferImageCopy dst_copy = {
      .imageSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
      .imageExtent = {image_data.width, image_data.height, 1},
   };

   vkCmdCopyImageToBuffer(cmd_buf, dst_img.image(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dst_buf.buffer(), 1, &dst_copy);

   vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, 0, NULL, 0, NULL, 0, NULL);

   if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
         throw -1;

   VkFence fence;
   const VkFenceCreateInfo fence_info = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
   };
   VkResult result = vkCreateFence(dev.dev(), &fence_info, nullptr, &fence);
   if (result != VK_SUCCESS)
      throw -1;

   const VkSubmitInfo submit_info = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers = &cmd_buf
   };
   vkQueueSubmit(dev.gfx_queue(), 1, &submit_info, fence);

   result = vkWaitForFences(dev.dev(), 1, &fence, true, 1000000000);
   if (result != VK_SUCCESS)
      throw -1;

   printf("success!\n");

   unsigned char *output = (unsigned char*)dst_buf.data();
   std::ofstream out("decoded.ppm");
   out << "P3\n" << image_data.width << " " << image_data.height << "\n255\n";
   for (unsigned y = 0; y < image_data.height; ++y) {
     for (unsigned x = 0; x < image_data.width; ++x) {
        for (int c = 0; c < 3; ++c) {
            if (x || c)
               out << " ";
            out << (unsigned)output[(y * image_data.width + x) * 4 + c];
        }
     }
     out << "\n";
   }
   return 0;
}
