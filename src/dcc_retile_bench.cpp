/*
 * Copyright © 2020 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <stdio.h>
#include <time.h>

double gettime() {
   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC, &ts);
   return ts.tv_sec + ts.tv_nsec/1e9;
}

int main() {
   Device dev;

   VkDrmFormatModifierPropertiesListEXT mod_prop_list = {
      .sType = VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT,
   };

   VkFormatProperties2 format_props = {
     .sType = VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2,
     .pNext = &mod_prop_list
   };
  
   vkGetPhysicalDeviceFormatProperties2(dev.pdev(), VK_FORMAT_R8G8B8A8_UNORM, &format_props);

   std::vector<VkDrmFormatModifierPropertiesEXT> mod_props(mod_prop_list.drmFormatModifierCount);
   mod_prop_list.pDrmFormatModifierProperties = mod_props.data();
   vkGetPhysicalDeviceFormatProperties2(dev.pdev(), VK_FORMAT_R8G8B8A8_UNORM, &format_props);

   VkDrmFormatModifierPropertiesEXT mod = {};
   for (auto mod_prop : mod_props) {
      if ((mod_prop.drmFormatModifier >> 14) & 1)
         mod = mod_prop;
   }
   if (mod.drmFormatModifier == 0) {
      fprintf(stderr, "Failed to find a retiling modifier\n");
      return 0;
   }

   VkImageDrmFormatModifierListCreateInfoEXT mod_list = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT,
      .drmFormatModifierCount = 1,
      .pDrmFormatModifiers = &mod.drmFormatModifier,
   };

   const unsigned width = 1920;
   const unsigned height = 1080;
   const VkImageCreateInfo img_create_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
         .pNext = &mod_list,
         .imageType = VK_IMAGE_TYPE_2D,
         .format = VK_FORMAT_R8G8B8A8_UNORM,
         .extent = {width, height, 1},
         .mipLevels = 1,
         .arrayLayers = 1,
         .samples = VK_SAMPLE_COUNT_1_BIT,
         .tiling = VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT,
         .usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
         .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
   };

   Image image(dev, img_create_info, Access::gpu);
      

   const unsigned transition_count = 10000;
   std::vector<VkCommandBuffer> cmd_buffers(16);
   for (auto& cmd_buf : cmd_buffers) {
      cmd_buf = create_cmdbuf(dev, dev.gfx_pool());
      
      const VkCommandBufferBeginInfo begin_info = {
         .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
      };
      vkBeginCommandBuffer(cmd_buf, &begin_info);

      VkImageMemoryBarrier init_barrier =
               {
                  .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                  .srcAccessMask = 0,
                  .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                  .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                  .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                  .srcQueueFamilyIndex = 0,
                  .dstQueueFamilyIndex = 0,
                  .image = image.image(),
                  .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
               };
      vkCmdPipelineBarrier(cmd_buf, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, NULL, 0, NULL, 1, &init_barrier);

      for (unsigned i = 0; i < transition_count; ++i) {
               VkImageMemoryBarrier export_barrier =
               {
                  .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                  .srcAccessMask = 0,
                  .dstAccessMask = 0,
                  .oldLayout = VK_IMAGE_LAYOUT_GENERAL,
                  .newLayout = VK_IMAGE_LAYOUT_GENERAL,
                  .srcQueueFamilyIndex = 0,
                  .dstQueueFamilyIndex = VK_QUEUE_FAMILY_FOREIGN_EXT,
                  .image = image.image(),
                  .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}
               };
               vkCmdPipelineBarrier(cmd_buf, 0, 0, 0, 0, NULL, 0, NULL, 1, &export_barrier);
      }

      if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
         throw -1;
   }


   std::vector<VkFence> fences(cmd_buffers.size());
   for (auto& f : fences) {
      const VkFenceCreateInfo fence_info = {
         .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
         .flags = VK_FENCE_CREATE_SIGNALED_BIT,
      };
      VkResult result = vkCreateFence(dev.dev(), &fence_info, nullptr, &f);
      if (result != VK_SUCCESS)
         throw -1;
   }

   unsigned idx = 0;
   std::vector<double> times(cmd_buffers.size());
   const unsigned total_iter = 300;
   for (unsigned i = 0; i < total_iter; ++i) {

      VkResult result = vkWaitForFences(dev.dev(), 1, &fences[idx], true, 1000000000);
      if (result != VK_SUCCESS)
         throw -1;

      double t = gettime();
      if (times[idx]  > 0.0) {
         double diff = t - times[idx];
         if (total_iter - i <= 10)
            fprintf(stderr, "time for full iter: %f s %f ns/barrier %f ns/pixel\n", diff, diff*1e9/transition_count, diff*1e9/transition_count/width/height);
      }
      times[idx] = t;
      vkResetFences(dev.dev(), 1, &fences[idx]);

      const VkSubmitInfo submit_info = {
         .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
         .commandBufferCount = 1,
         .pCommandBuffers = &cmd_buffers[idx]
      };
      vkQueueSubmit(dev.gfx_queue(), 1, &submit_info, fences[idx]);
      idx = (idx + 1) % cmd_buffers.size();
   }
   
   printf("success!\n");
   return 0;
}
