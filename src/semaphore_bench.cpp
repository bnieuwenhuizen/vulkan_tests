/*
 * Copyright © 2023 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.h"

#include <stdio.h>
#include <thread>
#include <unistd.h>

int main() {
  Device dev;

  if (dev.num_compute_queues() < 2) {
    printf("Need at least 2 compute queues\n");
    return 1;
  }

  VkSemaphore sem;
  VkSemaphoreCreateInfo sci = {.sType =
                                   VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
  vkCreateSemaphore(dev.dev(), &sci, NULL, &sem);

  VkEvent event;
  VkEventCreateInfo eci = {.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO};
  vkCreateEvent(dev.dev(), &eci, NULL, &event);

  const unsigned num_submissions = 5;
  VkQueryPool qp;
  VkQueryPoolCreateInfo qpci = {.sType =
                                    VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
                                .queryType = VK_QUERY_TYPE_TIMESTAMP,
                                .queryCount = num_submissions};
  vkCreateQueryPool(dev.dev(), &qpci, NULL, &qp);

  std::vector<VkCommandBuffer> cmd_buffers(num_submissions);
  VkCommandBufferAllocateInfo cbai = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = dev.compute_pool(),
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = num_submissions};
  vkAllocateCommandBuffers(dev.dev(), &cbai, cmd_buffers.data());

  for (unsigned i = 0; i < num_submissions; ++i) {
    VkCommandBufferBeginInfo cbbi = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = 0,
    };
    vkBeginCommandBuffer(cmd_buffers[i], &cbbi);

    if (i == 0) {
      vkCmdWaitEvents(
          cmd_buffers[i], 1, &event, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, NULL, 0, NULL, 0, NULL);
    }
    vkCmdWriteTimestamp(cmd_buffers[i], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, qp,
                        i);

    vkEndCommandBuffer(cmd_buffers[i]);
  }

  for (int j = 0; j < 100; ++j) {
    vkResetQueryPool(dev.dev(), qp, 0, num_submissions);

    vkResetEvent(dev.dev(), event);

    std::thread t{[&]() {
      usleep(1000);
      vkSetEvent(dev.dev(), event);
    }};
    for (unsigned i = 0; i < cmd_buffers.size(); i++) {
      VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
      VkSubmitInfo submit_info = {
          .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
          .waitSemaphoreCount = i > 0 ? 1u : 0u,
          .pWaitSemaphores = &sem,
          .pWaitDstStageMask = &wait_stage,
          .commandBufferCount = 1,
          .pCommandBuffers = &cmd_buffers[i],
          .signalSemaphoreCount = i + 1 == cmd_buffers.size() ? 0u : 1u,
          .pSignalSemaphores = &sem,
      };

      vkQueueSubmit(dev.compute_queue(i & 1), 1, &submit_info, VK_NULL_HANDLE);
    }
    t.join();

    vkDeviceWaitIdle(dev.dev());

    uint64_t timestamps[num_submissions];
    vkGetQueryPoolResults(dev.dev(), qp, 0, num_submissions, sizeof(timestamps),
                          timestamps, sizeof(uint64_t),
                          VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WAIT_BIT);
    printf("iter\n");
    for (unsigned i = 1; i < num_submissions; ++i) {
      double diff =
          (timestamps[i] - timestamps[i - 1]) * dev.timestamp_period() / 1e3;

      printf("  %d: %f us\n", i, diff);
    }
  }

  return 0;
}